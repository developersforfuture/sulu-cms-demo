<?php

declare(strict_types=1);

namespace App\Tests\Functional\Templates\Pages;

use App\Tests\Functional\BaseTestCase;
use App\Tests\Functional\Traits\PageTrait;

/**
 * Test homepage template.
 */
class HomepageTest extends BaseTestCase
{
    use PageTrait;

    /**
     * {@inheritdoc}
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->initPhpcr();
    }

    /**
     * {@inheritdoc}
     */
    public function testHomepage(): void
    {
        $locale = 'en';

        $this->createPage(
            'homepage',
            'example',
            [
                'title' => 'Neue Homepage',
                'url' => '/homepage',
                'published' => true,
            ],
            $locale
        );

        $client = $this->createWebsiteClient();
        $client->setServerParameter('HTTP_HOST', 'example.lo');
        $crawler = $client->request('GET', '/homepage');

        $this->assertHttpStatusCode(200, $client->getResponse());
        $this->assertCrawlerProperty($crawler, 'title', 'Neue Homepage');
    }
}
