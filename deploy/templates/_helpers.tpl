{{- define "imagePullSecret" }}
{{- printf "{\"auths\":{\"%s\":{\"Username\":\"%s\",\"Password\":\"%s\",\"Email\":\"%s\"}}}" .Values.imageCredentials.registry .Values.imageCredentials.username .Values.imageCredentials.password .Values.imageCredentials.email | b64enc }}
{{- end }}

{{- define "mediaproxy.fullname" -}}
    {{- printf "%s-%s" .Release.Name "mediaproxy" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "elasticsearch.fullname" -}}
    {{- printf "%s-%s" .Release.Name "elasticsearch" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "jackrabbit.fullname" -}}
    {{- printf "%s-%s" .Release.Name "jackrabbit" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "taskrunner.fullname" -}}
    {{- printf "%s-%s" .Release.Name "taskrunner" | trunc 63 | trimSuffix "-" -}}
{{- end -}}
