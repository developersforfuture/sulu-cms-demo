# Deployment

## Local Requirements

### Kubernetes-Shell (kubectl)

Kubectl is a command line interface for running commands against Kubernetes clusters. This overview covers the syntax of 
kubectl, describes the command operations and provides common examples. For details about each command, including all 
the supported flags and subcommands, see the kubectl reference documentation.

For installation instructions see following link.

https://kubernetes.io/docs/tasks/tools/install-kubectl/

### Helm

Helm is a tool that streamlines installing and managing Kubernetes applications and resources. Think of it like 
apt/yum/homebrew for Kubernetes. Using helm charts is recommended for installing resources is recommended, because they 
are maintained and typically kept up-to-date by the Kubernetes community.

* Helm has two parts: a client (helm) and a server (tiller)
* Tiller runs inside of your Kubernetes cluster and manages releases (installations) of your helm charts.
* Helm runs on your laptop, CI/CD, or in our case, the Cloud Shell.

For installation instructions see following link.

https://docs.helm.sh/using_helm/#installing-helm

### CLOUD SDK (gcloud) optional

The Google Cloud SDK contains tools and libraries that allow to create and manage resources on 
Google's Cloud Platform, including App Engine, Compute Engine, Cloud Storage, Cloud SQL, and BigQuery.

For installation instructions see following link.

https://cloud.google.com/sdk/downloads

## Cluster Requirement

### Cert-Manager

cert-manager is a Kubernetes addon to automate the management and issuance of TLS certificates from various issuing sources.

It will ensure certificates are valid and up to date periodically, and attempt to renew certificates at an appropriate time before expiry.

https://github.com/jetstack/cert-manager/tree/master/deploy/charts/cert-manager

### Nginx Ingress

nginx-ingress is an Ingress controller that uses ConfigMap to store the nginx configuration.

To use, add the kubernetes.io/ingress.class: nginx annotation to your Ingress resources.

https://github.com/helm/charts/tree/master/stable/nginx-ingress

## Preparation

1. Copy `secret.prod.yaml.dist` to `secret.prod.yaml`
2. Copy `secret.stage.yaml.dist` to `secret.stage.yaml`
3. Find and replace all `<deploy-prod-name>` and `<deploy-stage-name>` except `secret.prod.yaml.dist` and `secret.stage.yaml.dist`
4. Find and replace all `???` with real values in your context except `secret.prod.yaml.dist` and `secret.stage.yaml.dist`
5. Create in gitlab a CI/CD variable named `SECRET_PROD_VALUES` with the content of `secret.prod.yaml`
6. Create in gitlab a CI/CD variable named `SECRET_STAGE_VALUES` with the content of `secret.stage.yaml`
7. Deploy it locally one time
